package by.rexy.centcontrol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CentcontrolApplication {

    public static void main(String[] args) {
        SpringApplication.run(CentcontrolApplication.class, args);
    }

}
